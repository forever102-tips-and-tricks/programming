# General:

Basic knowledge when using flutter.

## Common CLI:
- Open iOS simulator:
```bash
open -a Simulator # Then you can select the device you want to test by selecting menu `Hardware/Device/iOS`.
```

- Create a flutter project and run it:
```bash
flutter create first_app # NOTE: the `-` character is not allowed.

# Run it.
flutter run
# OR:
flutter run -d <device_id> # Use this if you have multiple devices connected.
```

> **NOTE:** 
>> On `GitLab`: To avoid potential issues and make it consistent, when you name the app on `GitLab`, you should use `_` instead of `-` or `space`. That's because I notice that `GitLab` use the `-` to name the app while `flutter` uses `_`.<br>
>> App's name: To make sure the app's name is the same, open `pubspec.yaml` file to check its name.

- Open Android simulator: Open `Android Studio`, then select menu `Tools/AVD` -> Click the `play` icon of the device you want to open. -> Run the app using `flutter run`.

## Upgrade and downgrade Flutter SDK.

Before upgrading or downgrading, make sure there's no changes in the `Applications/flutter` folder. Otherwise, you could the error `Unable to upgrade ...`.
- To upgrade run `flutter upgrade`.
- To downgrade run `flutter version <version>`.

--------------------------------------------------

# Sample `README.md` file
You can use the following instructions to make the app up and running when creating `README.md` file.

## Prerequisites

Make sure you have `flutter` installed on your machine and no issues shown when running cmd `flutter doctor`.

Clone the repo from `GitLab` and run the following cmd:
```bash
flutter pub get # To install flutter and 3rd party libraries. Side-note: what does `pub` stand for? => One user on `stackOverflow` explains that's because the best place to play `Dart` is in the pub. :))
# Or:
flutter packages get # This makes more sense than `pub`.
```

## Make it up and running on emulators.

Using `CLI`:
- To list the available emulators on your machine:
```bash
flutter emulators

# In my case, it shows the following available emulators:
Nexus_5X_API_29_x86 • Nexus 5X API 29 x86 • Google • android
Pixel_2_API_27      • Pixel 2 API 27      • Google • android
apple_ios_simulator • iOS Simulator       • Apple  • ios
```

- Pick the emulator of your choice (I choose `apple_ios_simulator`) and run:
```bash
flutter emulators --launch apple_ios_simulator # This will launch the iOS emulator.
flutter run # This runs the app on emulator.

# Or run on both emulators iOS and Android: open all emulators and run the following cmd:
flutter run -d all
```


Using `VS Code`:
- Press `Ctrl + F5` -> It shows `Select Environment`, choose `Dart & Flutter`.
- Then it shows `Select a device to use`, choose an emulator of your choice.

## Make it up and running on real devices.

Running on iOS devices using `xCode`:
- Open the app in `xCode`.
- Choose your real device and click run.
- If you see any configuration errors (often happen in `xCode`), try to search `google` to fix it. For ex, I got the following errors:
    - `bundle identifier` error: then I changed its value into another one of `com.example.udemy.personalExpensesApp`.
> **NOTE:** It's better to run the app in `xCode` first, then you can use the `CLI` to run it without using `xCode`. That's because sometimes we need to fix some configurations in `xCode` such as `bundle identifier`, `provisioning profile` and `signing certificate`.

Running on iOS devices using `CLI`:
- To avoid potential issues, make sure you're running the latest version of `Mac OS` and `xCode`.
- Make sure you already register an `Apple Account` and add it to `xCode`.
- Plug-in your iOS device.
- Run `flutter devices` to see if it shows your connected device. For ex, I got:
```bash
Tuan’s iPhone 7 • <your_device_id> • ios • iOS 12.4.1
```
- Copy your device id and run `flutter run -d <your_device_id>`.

Running on Android devices:
- ToDo: Find a Android device.

--------------------------------------------------

# Good resources: